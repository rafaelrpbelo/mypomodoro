class MyPomodoro::Timer
  DEFAULT_SLEEP_TIME = 2

  def initialize(time_in_seconds)
    @start_time = Time.now
    @remaining_seconds = time_in_seconds
  end

  def run
    Thread.current[:start_time] = start_time

    while remaining_seconds >= 0 do
      puts "tick: #{remaining_seconds}"
      sleep 1

      if Thread.current[:pause] == true
        Thread.stop
      end

      @remaining_seconds -= 1
      Thread.current[:remaining_seconds] = remaining_seconds
    end

    yield(Time.now) if block_given?
  end

  private

  attr_reader :start_time, :remaining_seconds
end
