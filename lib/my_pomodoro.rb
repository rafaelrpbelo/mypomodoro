require 'optparse'

class MyPomodoro
  require 'lib/my_pomodoro/timer'

  DEFAULT_TIME_IN_MINUTES = 20

  def initialize(params={})
    @time = params.fetch(:time, DEFAULT_TIME_IN_MINUTES)
  end

  def run
    run_timer.join
  end

  def console
    run_timer

    while @timer.alive?
      puts '1 - Pause'
      puts '2 - Resume'
      puts '3 - Remaining seconds'
      puts '4 - Exit'
      puts
      puts 'Option: '
      opt = gets.strip

      case opt
      when '1'
        if @timer[:pause] == true
          puts 'Timer already paused'
        else
          @timer[:pause] = true
        end
      when '2'
        if @timer[:pause] == false
          puts 'Timer already resumed'
        else
          @timer[:pause] = false
          @timer.run
        end
      when '3'
        puts "#{@timer[:remaining_seconds]} seconds is remaining"
      when '4'
        exit(1)
      else
        puts 'Invalid option!'
      end
    end
  end

  def self.run(params={})
    new(params).run
  end

  def self.console(params={})
    new(params).console
  end

  private

  attr_reader :time, :timer

  def run_timer
    @timer ||= Thread.new do
      Thread.current[:pause] = false

      t = Timer.new(time_in_seconds)

      t.run do |time|
        notify_times_up!
      end

      puts 'FINISHED'
    end
  end

  def time_in_seconds
    time * 60
  end

  def notify_times_up!
    system(%(notify-send \
      --urgency=critical \
      --app-name="MyPomodoro" \
      "Time's up!" \
      "You did it!"))
  end
end
